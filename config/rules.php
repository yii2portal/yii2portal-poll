<?php


return [
    [
        'pattern' => "",
        'route' => "index/index",
        'defaults' => [
            'page' => 1
        ]
    ],
    [
        'pattern' => "page_<page:\d+>",
        'route' => "index/index",
        'defaults' => [
            'page' => 1
        ]
    ],
    [
        'pattern' => "<item_id:\d+>_<translit>/",
        'route' => "index/view",
    ]
];