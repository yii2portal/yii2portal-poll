<?php

namespace yii2portal\poll;

use Yii;
use yii2portal\poll\models\Poll;
use \yii2portal\poll\components\cplugin\Poll as PollCplugin;

class Module extends \yii2portal\core\Module
{

    public $controllerNamespace = 'yii2portal\poll\controllers';

    private $_votedPolls = null;

    public function init()
    {
        parent::init();
        Yii::$app->getModule('cplugin')->registerPlugin(PollCplugin::className());
    }

    public function getVotedPolls($ip){
        if($this->_votedPolls === null) {
            $ip2long = ip2long($ip);
            $this->_votedPolls = Poll::find()->joinWith([
                'pollLog' => function ($query) use ($ip2long) {
                    $query->andWhere([
                        'poll_log.ip' => $ip2long
                    ]);
                }
            ])->all();
        }

        return $this->_votedPolls;
    }

    public function isVoted($pollId, $ip){
        $votedPolls = $this->getVotedPolls($ip);
        $polls = array_filter($votedPolls,function($pollItem) use ($pollId){
            return $pollId == $pollItem->id;
        });

        return empty($polls)?false:true;
    }


}