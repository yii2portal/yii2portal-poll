<?php

namespace yii2portal\poll\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii2portal\core\controllers\Controller;
use yii2portal\poll\models\Poll;
use yii2portal\poll\models\PollForm;
use yii2portal\poll\models\PollLog;

/**
 * Site controller
 */
class IndexController extends Controller
{

    public function actionIndex($structure_id)
    {

        $query = Poll::find()->where([
            'is_del' => 0
        ]);

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
//                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'date_from' => SORT_DESC
                ]
            ],
        ]);

        return $this->render('index', [
            'page' => Yii::$app->modules['structure']->getPage($structure_id),
            'provider' => $provider
        ]);
    }

    public function actionView($structure_id, $item_id)
    {


        $poll = Poll::findOne($item_id);
        if (!$poll) {
            throw new NotFoundHttpException();
        }



        $form = new PollForm([
            'poll' => $poll,
        ]);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {

            $els = is_array($form->answer) ? $form->answer: [$form->answer];

            $insertAnswers = [];

            $pollStatAnswers = $poll->answers;


            foreach ($els as $el) {
                $insertAnswers [] = [
                    'pid' => $poll->id,
                    'ip' => ip2long(Yii::$app->request->getUserIP()),
                    'answer_id' => $el,
                    'answer' => '',
                    'dateline' => time()
                ];
                $pollStatAnswers[$el]['count'] +=1;
            }

            Yii::$app->db->createCommand()->batchInsert(PollLog::tableName(), [
                'pid', 'ip', 'answer_id', 'answer', 'dateline'
            ], $insertAnswers)->execute();

            $poll->answer = serialize($pollStatAnswers);
            $poll->save();

            return  $this->redirect(Yii::$app->request->get('referrer', $poll->urlPath));

        }

        return $this->render('view', [
            'category' => Yii::$app->modules['structure']->getPage($structure_id),
            'poll' => $poll,
            'form' => $form,
        ]);
    }


}
