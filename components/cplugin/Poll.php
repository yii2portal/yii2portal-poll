<?php

namespace yii2portal\poll\components\cplugin;

use yii2portal\cplugin\models\Cplugin;
use yii2portal\poll\models\Poll as PollModel;
use yii2portal\poll\models\PollForm;

class Poll extends Cplugin
{

    public function pluginChk($params)
    {
        return array(
            'status' => true,
            'params' => $params
        );
    }

    public function pluginConfig($params)
    {

        return array(
            'html' => $this->render('config', array('params' => $params)),
            'config' => true,
            'resizable' => false,
            'styles' => false
        );
    }

    public function pluginRender($params)
    {
        $return = '';

        $id = $params['id'];

        $poll = PollModel::findOne($id);

        if (!empty($poll)) {
            $params['width'] = $this->getSize($params['width']);

            $form = new PollForm([
                'poll' => $poll,
            ]);

            $return = $this->render('poll', [
                'poll' => $poll,
                'params' => $params,
                'form' => $form,
            ]);
        }


        return $return;
    }
}