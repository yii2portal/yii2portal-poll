<?php


namespace yii2portal\poll\components\widget;

use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii2portal\core\components\Widget as Yii2PortalWidget;
use yii2portal\poll\models\Poll;
use yii2portal\poll\models\PollForm;


class Widget extends Yii2PortalWidget
{

    public function insert($view)
    {

        $polls = Poll::find()
            ->andActiveForIP(Yii::$app->request->getUserIP())
            ->andWhere([
                'is_del' => '0'
            ])
            ->andWhere(['between',
                new Expression("UNIX_TIMESTAMP()"),
                new Expression("date_from"),
                new Expression('date_to')
            ])
            ->andWhere('struct_sel="" or FIND_IN_SET(:pageId, struct_sel)',[
                'pageId'=>Yii::$app->getModule('structure')->getCurrentPage()->id
            ])
            ->orderBy(['poll.id' => SORT_DESC])
            ->limit(10)
            ->all();

        if (!empty($polls)) {
            shuffle($polls);
        }

        $poll = array_shift($polls);

        $form = null;
        if($poll) {
            $form = new PollForm([
                'poll' => $poll
            ]);
        }


        return $this->render($view, [
            'poll' => $poll,
            'form'=>$form
        ]);

    }

}