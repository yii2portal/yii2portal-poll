<?php

namespace yii2portal\poll\models;

use Yii;

/**
 * This is the model class for table "poll_log".
 *
 * @property integer $id
 * @property integer $pid
 * @property string $ip
 * @property integer $answer_id
 * @property string $answer
 * @property integer $dateline
 * @property string $username
 * @property string $user_comment
 * @property integer $is_view
 */
class PollLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'poll_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pid', 'ip', 'answer_id', 'dateline', 'is_view'], 'integer'],
            [['username', 'user_comment', 'is_view'], 'required'],
            [['user_comment'], 'string'],
            [['answer'], 'string', 'max' => 255],
            [['username'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pid' => 'Pid',
            'ip' => 'Ip',
            'answer_id' => 'Answer ID',
            'answer' => 'Answer',
            'dateline' => 'Dateline',
            'username' => 'Username',
            'user_comment' => 'User Comment',
            'is_view' => 'Is View',
        ];
    }

    /**
     * @inheritdoc
     * @return PollLogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PollLogQuery(get_called_class());
    }
}
