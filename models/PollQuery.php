<?php

namespace yii2portal\poll\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the ActiveQuery class for [[Poll]].
 *
 * @see Poll
 */
class PollQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Poll[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Poll|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }


    public function andActiveForIP($ip)
    {
        $votedPolls = Yii::$app->getModule('poll')->getVotedPolls($ip);
        if (!empty($votedPolls)) {
            return $this->andWhere(['not in', 'id', ArrayHelper::getColumn($votedPolls, 'id')]);
        } else {
            return $this;
        }
    }
}
