<?php

namespace yii2portal\poll\models;

/**
 * This is the ActiveQuery class for [[PollLog]].
 *
 * @see PollLog
 */
class PollLogQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return PollLog[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PollLog|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
