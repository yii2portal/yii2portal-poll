<?php

namespace yii2portal\poll\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "poll".
 *
 * @property integer $id
 * @property integer $cat_id
 * @property integer $view
 * @property string $question
 * @property string $description
 * @property string $answer
 * @property integer $date_from
 * @property integer $date_to
 * @property string $type_cr
 * @property integer $count_min
 * @property integer $count_max
 * @property integer $add_comment
 * @property integer $is_del
 * @property string $olang
 * @property string $struct_sel
 */
class Poll extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'poll';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cat_id', 'question', 'add_comment'], 'required'],
            [['cat_id', 'view', 'date_from', 'date_to', 'count_min', 'count_max', 'add_comment', 'is_del'], 'integer'],
            [['description', 'answer', 'type_cr'], 'string'],
            [['question', 'struct_sel'], 'string', 'max' => 255],
            [['olang'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cat_id' => 'Cat ID',
            'view' => 'View',
            'question' => 'Question',
            'description' => 'Description',
            'answer' => 'Answer',
            'date_from' => 'Date From',
            'date_to' => 'Date To',
            'type_cr' => 'Type Cr',
            'count_min' => 'Count Min',
            'count_max' => 'Count Max',
            'add_comment' => 'Add Comment',
            'is_del' => 'Is Del',
            'olang' => 'Olang',
            'struct_sel' => 'Struct Sel',
        ];
    }

    /**
     * @inheritdoc
     * @return PollQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PollQuery(get_called_class());
    }


    public function getPollLog()
    {
        return $this->hasMany(PollLog::className(), ['pid' => 'id']);
    }

    public function getUrlPath()
    {
        $structure = Yii::$app->modules['structure'];

        return $structure->getUrlByModule('poll') . "{$this->id}_" . $structure->makeUrl($this->question) . '/';

    }

    public function getIsActive($ip = null)
    {
        $ip = !$ip ? Yii::$app->request->getUserIP() : $ip;
        
        return (
            time() > $this->date_from &&
            time() < $this->date_to &&
            !Yii::$app->getModule('poll')->isVoted($this->id, $ip)
        );
    }

    public function getIsDate()
    {
        return (time() > $this->date_to);
    }

    public function getAnswers()
    {
        $answers = unserialize($this->answer);

        $summ = 0;
        foreach ($answers as $key => $one) {
            $answers[$key]['id'] = $key;
            $answers[$key]['title'] = (get_magic_quotes_gpc() ? stripcslashes($answers[$key]['title']) : $answers[$key]['title']);
            $summ += intval($answers[$key]['count']);
        }
        foreach ($answers as $key => $one) {
            $answers[$key]['proc'] = $summ > 0 ? floor(intval($answers[$key]['count']) * 100 / $summ) : 0;
        }

        return $answers;
    }

    public function getAnswersCount()
    {
        $answers = $this->answers;
        return array_sum(ArrayHelper::getColumn($answers, 'count'));
    }
}
