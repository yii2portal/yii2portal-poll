<?php

namespace yii2portal\poll\models;

use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "poll".
 *
 * @property integer $poll_id
 * @property array $el[]
 */
class PollForm extends Model
{
    const POLL_RADIO = 1;
    const POLL_CHECKBOX = 2;

    /**
     * @var Poll
     */
    public $poll; //// ->type_cr == 'radio' ? PollForm::POLL_RADIO : PollForm::POLL_CHECKBOX,
    public $answer;
//    public $el;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['answer'], 'required'],
            ['answer', 'validateIsActive'],
            ['answer', 'validateAnswer'],
        ];
    }

    public function validateIsActive($attribute, $params)
    {
        if (!$this->poll->isActive) {
            $this->addError($attribute, 'Голосование неактивно');
        }
    }

    public function validateAnswer($attribute, $params)
    {
        $answers = $this->answers;
        if ($this->poll->type_cr == 'radio') {
            if(!isset($answers[$this->answer])) {
                $this->addError($attribute, 'Такого ответа не существует');
            }
        }else{
            foreach($this->answer as $el){
                if(!isset($answers[$el])) {
                    $this->addError($attribute, 'Такого ответа не существует');
                    break;
                }
            }

            if(count($this->answer) <$this->poll->count_min){
                $this->addError($attribute, "Максимально допустимое количество ответов - {$this->poll->count_min}");
            }
            if(count($this->answer) >$this->poll->count_max){
                $this->addError($attribute, "Максимально допустимое количество ответов - {$this->poll->count_max}");
            }

        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'answer' => 'Ответы'
        ];
    }


    public function getAnswers(){
        return ArrayHelper::map($this->poll->answers, 'id', 'title');
    }

}
